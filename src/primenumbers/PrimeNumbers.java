package primenumbers;

import java.util.Scanner;

public class PrimeNumbers {

    public static void main(String[] args) {
        int number = 0;
        boolean isPrime = true;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number!");
        number = sc.nextInt();
        sc.close();
        
        if (number < 1) {
            System.out.println("Illegal paramter!");
        } else {
            for (int i = 2; i < number; i++) {
                if (number % i == 0) {
                    isPrime = false;
                    break; // for
                }
            }
            if (isPrime) {
                System.out.println("The number " + number + " is a prime.");
            } else {
                System.out.println("The number " + number + " is NOT a prime.");
            }
        }
    }
}
