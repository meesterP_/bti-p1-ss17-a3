package primenumbers;

import java.util.Scanner;

public class PrimeNumberPrinter {

    public static void main(String[] args) {
        int index = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number!");
        index = sc.nextInt();
        sc.close();
        System.out.println();

        if (index > 1) {
            for (int currentNumber = 2; currentNumber <= index; currentNumber++) {
                if (isPrime(currentNumber)) {
                    System.out.println(currentNumber);
                }
            }
        } else {
            System.out.println("Illegal paramter!");
        }
    }

    private static boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

