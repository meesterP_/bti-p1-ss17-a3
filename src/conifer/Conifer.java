package conifer;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Conifer {
    private static int height = 0;
    private static int width = 0;


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number!");
        height = sc.nextInt();
        sc.close();
        System.out.println();
        width = height * 2 - 1;

        for (int row = 0; row < height; row++) {
            System.out.println(createRowString(row));
        }
    }

    private static String createRowString(int row) {
        String rowString = "";
        for (int i = 0; i < width; i++) {
            if (i < height - 1 - row || height - 1 + row < i) {
                rowString += ".";
            } else if (i == height - 1) {
                rowString += "0";
            } else {
                rowString += "#";
            }
        }
        if ((isPrime(row))) {
            int leafToChange = 0;
            do {
                leafToChange = ThreadLocalRandom.current().nextInt(height - 1 - row, height - 1 + row + 1);
            } while (leafToChange == height - 1);
            StringBuilder rowStringSB = new StringBuilder(rowString);
            rowStringSB.setCharAt(leafToChange, 'i');
            rowString = rowStringSB.toString();
        }
        return rowString;
    }

    private static boolean isPrime(int number) {
        if (number < 2) {
            return false;
        }
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
